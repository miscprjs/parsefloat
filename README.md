# parsefloat

## What is it

Parsefloat is a small library 
for converting string to float32 w/o stdlib.

See doc/ directory for detailed requirement
specification.

## Usage

A small example can be found in src/main.c:

    float result;
    const char* value = "1e3";
    uint32_t r = str2float(value, &result);
    printf(
      "string=\"%s\" value=%f message=\"%s\" \n",
      value, result, getErrorMessage(r)
    );

Compile:
 
    make

Run:

    ./parsefloat


## Tests

There are tests for various values
provided in various formats, 
and also for Inf and NaN.

The unit test is using Acutest framework:
https://github.com/mity/acutest

Compile:
 
    make test

Run:

    ./test

The test can run individual tests,
or select output format. Short info:

    ./test --help

See Acutest documentation for
detailed information.

## Implementation notes

* The str2float() function is pretty long, should be split.

* The calcPower() function is a naive implementation, it should be optimized.

* The figure doc/statemachine.jpg explains the states of the parsing process.