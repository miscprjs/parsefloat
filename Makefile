CC = clang
CFLAGS = -O2 -g -c
SRC = src
OBJ = /tmp/obj-parsefloat
OBJDIR = ${OBJ}/.dir

parsefloat: \
			${OBJ}/str2float.o \
			${OBJ}/main.o
	${CC} \
		${OBJ}/str2float.o \
		${OBJ}/main.o \
		-o parsefloat

test: \
			Makefile \
			${OBJ}/str2float.o \
			${OBJ}/test.o
	${CC} \
		${OBJ}/str2float.o \
		${OBJ}/test.o \
		-o test

${OBJ}/str2float.o: \
			${OBJDIR} \
			${SRC}/str2float.c \
			${SRC}/str2float.h
	${CC} ${CFLAGS} \
		${SRC}/str2float.c \
		-o ${OBJ}/str2float.o

${OBJ}/main.o: \
			${OBJDIR} \
			${SRC}/main.c \
			${SRC}/main.h
	${CC} ${CFLAGS} \
		${SRC}/main.c \
		-o ${OBJ}/main.o

${OBJ}/test.o: \
			${OBJDIR} \
			${SRC}/test.c 
	${CC} ${CFLAGS} \
		${SRC}/test.c \
		-o ${OBJ}/test.o

${OBJDIR}:
	mkdir -p ${OBJ}
	touch ${OBJDIR}

clean: ${OBJDIR}
	rm -rf ${OBJ}/*
	rm -rf test
	rm -rf parsefloat

all: clean parsefloat