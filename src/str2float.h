#ifndef _STR2FLOAT_H
#define _STR2FLOAT_H

#include <stdint.h>
#include <math.h>

/*!
* \brief Return codes for str2float()
*/
enum {
	S2FR_SUCCESS
	,S2FR_STRING_TOO_LONG
	,S2FR_INVALID_CHAR_INT
	,S2FR_INVALID_CHAR_FRAC
	,S2FR_INVALID_CHAR_CONST
	,S2FR_INVALID_CHAR_MANT
};

/*!
* \brief Maximum length of input string
*/
#define S2F_MAX_LEN 9999

/*!
* \brief Parsing states
*/
enum {
	S2FS_CHAR_BEGIN
	,S2FS_CONST
	,S2FS_CHAR_INT
	,S2FS_CHAR_FRAC
	,S2FS_MANT_BEGIN
	,S2FS_MANT_MIDDLE
};

/*!
* \param[in] s string input, null terminated
* \param[out] n number output, zero when error has occurred
* \return 0 when success, nonzero otherwise, see enum S2FR_*
*
* \brief
* Parses the C string s, interpreting its content as a
* floating point number and returns its value as a float.
*
* \details
* Supports decimal (23.1), engineering (23.1e2) and
* scientific notation (2.31e3), and inf and nan too.
* This function is not case sensitive for input string s.
*/
int32_t str2float(const char* s, float* n);

/*!
* \param[in] c character to process
* \return lowercased character
* \brief Converts character to lowercase, e.g. 'C' -> 'c'
*/
inline unsigned char locase(unsigned char c);

/*!
* \param[in] c charater (ASCII digit) to parse
* \return value, -1 for error
* \brief parse value of a digit
*/
inline int parseChar(unsigned char c);

/*!
* \param[in] code result code from str2float()
* \return short error message to print
*
* \brief returns error message for specified error code
*/
const char* getErrorMessage(unsigned int code);

/*!
* \param[in] base the base number
* \param[in] power the power to raised
* \return result of power
*
* \brief power implementation (naive, slow)
*/
float calcPower(float base, int32_t power);

#endif