#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "str2float.h"


int main() {

	float result;
	const char* value = "1e3";
	uint32_t r = str2float(value, &result);
	printf("string=\"%s\" value=%f message=\"%s\" \n", value, result, getErrorMessage(r));

	return 0;
} // main()