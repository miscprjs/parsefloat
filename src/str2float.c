#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include "str2float.h"


int32_t str2float(const char* str, float* result) {

	// prepare variables

	int state = S2FS_CHAR_BEGIN;
	*result = 0;

	float charResult = 0;
	bool charNeg = false;
	float charFracDigitMul = 1.0;

	static const char* constInfStr = "inf";
	static const char* constNanStr = "nan";
	const char* constPtr = NULL;
	int constCount = 0;

	int32_t mantResult = 0;
	bool mantNeg = false;

	// read characters in the loop, go through states

	int i = -1;
	while (i < S2F_MAX_LEN) {

		unsigned char c = str[++i];
		if (c == '\0') break;
		c = locase(c);
		int value;

		switch (state) {

			case S2FS_CHAR_BEGIN:

				state = S2FS_CHAR_INT;

				if (c == '+') continue;

				if (c == '-') {
					charNeg = true;
					continue;
				}

				if (c == constInfStr[0]) {
					state = S2FS_CONST;
					constPtr = constInfStr;
					constCount++;
					continue;
				}

				if (c == constNanStr[0]) {
					state = S2FS_CONST;
					constPtr = constNanStr;
					constCount++;
					continue;
				}

				// don't break
			
			case S2FS_CHAR_INT:
			
				if (c == '.') {
					state = S2FS_CHAR_FRAC;
					continue;
				}

				if (c == 'e') {
					state = S2FS_MANT_BEGIN;
					continue;
				}

				value = parseChar(c);
				if (value == -1) return S2FR_INVALID_CHAR_INT;

				charResult = charResult * 10 + (float)value;
				break;

			case S2FS_CONST:

				if (c != constPtr[constCount++]) {
					return S2FR_INVALID_CHAR_CONST;
				}
				break;
			
			case S2FS_CHAR_FRAC:

				if (c == 'e') {
					state = S2FS_MANT_BEGIN;
					continue;
				}

				int value = parseChar(c);
				if (value == -1) return S2FR_INVALID_CHAR_FRAC;

				charFracDigitMul = charFracDigitMul / 10;
				charResult = charResult + (charFracDigitMul * (float)value);
				break;

			case S2FS_MANT_BEGIN:

				if (c == '+') continue;

				if (c == '-') {
					mantNeg = true;
					continue;
				}

				// don't break

			case S2FS_MANT_MIDDLE:

				value = parseChar(c);
				if (value == -1) return S2FR_INVALID_CHAR_MANT;

				mantResult = (mantResult * 10) + value;
				break;

		} // switch state

	} // while parse

	// handle error	
	if (i >= S2F_MAX_LEN) {
		return S2FR_STRING_TOO_LONG;
	}

	// handle constants
	if (constCount > 0) {

		if (constPtr == constInfStr) {
			*result = INFINITY;
		} else {
			*result = NAN;
		}

		return S2FR_SUCCESS;
	}

	// put together the result
	if (mantNeg) mantResult = -mantResult;
	float m = calcPower(10.0, mantResult);
	*result = charResult * m;
	if (charNeg) *result = -*result;

	return S2FR_SUCCESS;
} // str2float()


unsigned char locase(unsigned char c) {

	if (('A' <= c) && (c <= 'Z')) {
		c ^= ('c' ^ 'C');
	}

	return c;
} // locase()


inline int parseChar(unsigned char c) {

	if (c < '0') return -1;
	if (c > '9') return -1;

	return (int)(c - '0');
} // parseChar()


const char* getErrorMessage(unsigned int code) {
	
	switch (code) {
		case S2FR_SUCCESS: 
			return "success";
		case S2FR_STRING_TOO_LONG: 
			return "string too long";
		case S2FR_INVALID_CHAR_INT: 
			return "invalid char in characteristics integer part";
		case  S2FR_INVALID_CHAR_FRAC:
			return "invalid char in characteristics fracion part";
		case  S2FR_INVALID_CHAR_CONST:
			return "invalid char in constant name";
		case S2FR_INVALID_CHAR_MANT:
			return "invalid char in mantissa";
		default: 
			return "unknown error";
	}

} // getErrorMessage()


float calcPower(float base, int32_t power) {

	float result = 1.0;

	if (power > 0) {
		for (int32_t i = 0; i < power; i++) {
			result *= base;
		}
	} 
	else {
		for (int32_t i = 0; i > power; i--) {
			result /= base;
		}		
	}

	return result;
} // calcPower()
