#include <math.h>
#include "acutest.h"
#include "str2float.h"

static float value;
uint32_t code;


void TEST_FLOAT(float ref, float epsilon) {

	TEST_CHECK( fabs(ref - value) <= epsilon );
	TEST_MSG("expected: %f, produced: %f", ref, value);

	TEST_CHECK(code == S2FR_SUCCESS);

} // TEST_FLOAT()


void test_1p2() {
	code = str2float("1.2", &value);
	TEST_FLOAT(1.2, 0.0001);
}

void test_m0p1() {
	code = str2float("-0.1", &value);
	TEST_FLOAT(-0.1, 0.0001);
}

void test_4p3e9() {
	code = str2float("4.3e9", &value);
	TEST_FLOAT(4.3e9, 0);
}

void test_m8e2() {
	code = str2float("-8e2", &value);
	TEST_FLOAT(-8e2, 0);
}

void test_p5() {
	code = str2float("+5", &value);
	TEST_FLOAT(+5, 0);
}
 
void test_zero() {
	code = str2float("0", &value);
	TEST_FLOAT(0, 0);
}

void test_inf() {

	code = str2float("Inf", &value);
	TEST_CHECK(isinf(value));

	code = str2float("inf", &value);
	TEST_CHECK(isinf(value));

	code = str2float("Info", &value);
	TEST_CHECK(value == 0.0);
	TEST_CHECK(code == S2FR_INVALID_CHAR_CONST);

	code = str2float("ynf", &value);
	TEST_CHECK(value == 0.0);
	TEST_CHECK(code == S2FR_INVALID_CHAR_INT);

}

void test_nan() {

	code = str2float("NaN", &value);
	TEST_CHECK(isnan(value));

	code = str2float("NAN", &value);
	TEST_CHECK(isnan(value));

	code = str2float("Nino", &value);
	TEST_CHECK(value == 0.0);
	TEST_CHECK(code == S2FR_INVALID_CHAR_CONST);

	code = str2float("Nas", &value);
	TEST_CHECK(value == 0.0);
	TEST_CHECK(code == S2FR_INVALID_CHAR_CONST);

}

void test_5zillion() {
	code = str2float("5000000000000000000", &value);
	TEST_FLOAT(5000000000000000000.0, 0);
}

void test_9999() {
	code = str2float("999999999999", &value);
	TEST_FLOAT(999999999999, 0);
}

void test_m12em39() {
	code = str2float("-12e-39", &value);
	TEST_FLOAT(-12e-39, 0.0001);
}


TEST_LIST = {
   { "1.2", test_1p2 },
   { "-0.1", test_1p2 },
   { "4.3e9", test_4p3e9 },
   { "-8e2", test_m8e2 },
   { "+5", test_p5 },
   { "0", test_zero },
   { "Inf", test_inf },
   { "NaN", test_nan },
   { "5000000000000000000", test_5zillion },
   { "999999999999", test_9999 },
   { "-12e-39", test_m12em39 },
   { NULL, NULL } 
};