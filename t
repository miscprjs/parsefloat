#!/bin/bash
clear

rm -rf test
make -j4 test
if [[ -f test ]]; then
  echo --
  ./test
fi
